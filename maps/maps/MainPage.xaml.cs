﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace maps
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        
        public Xamarin.Forms.Maps.MapType MapType { get; set; }
        public MainPage()
        {
            InitializeComponent();
            AddPins();
            
        }
        
        private void OnStreetClicked(object sender, EventArgs e)
        {
            MyMap.MapType = MapType.Street;
        }
        private void OnSatelliteClicked(object sender, EventArgs e)
        {
            MyMap.MapType = MapType.Satellite;
        }
        private void AddPins()
        {
            var position = new Position(33.228630, -117.291800); //latitude & longitude for my house
            var pin = new Pin
            {
                Type = PinType.Place,
                Position = position,
                Label = "My house",
                Address = "4736 Sandalwood Way, Oceanside, CA"
            };
            MyMap.Pins.Add(pin);

            var position2 = new Position(45.008340, -93.502340); //latitude & longitude for my old house
            var pin2 = new Pin
            {
                Type = PinType.Place,
                Position = position2,
                Label = "My old house",
                Address = "17515 26th Avenue N, Plymouth, MN"
            };
            MyMap.Pins.Add(pin2);

            var position3 = new Position(45.460938, -122.466232); //latitude & longitude for my sister's house
            var pin3 = new Pin
            {
                Type = PinType.Place,
                Position = position3,
                Label = "My sister's house",
                Address = "8520 SE 190th Drive, Damascus, OR"
            };
            MyMap.Pins.Add(pin3);

            var position4 = new Position(36.384699, -119.654999); //latitude & longitude for Bogan Grove tree farm
            var pin4 = new Pin
            {
                Type = PinType.Place,
                Position = position4,
                Label = "Bogan Grove",
                Address = "6994 N 11th Avenue, Hanford, CA"
            };
            MyMap.Pins.Add(pin4);
        }
        async void OnPickerClicked(object sender, EventArgs e)
        {
            //provides options for navigation, depending on which button pressed navigate to the corresponding pin latitude/longitude
            string action = await DisplayActionSheet("Where would you like to go?", "Cancel", null, "My house", "My old house", "My sister's house", "Bogan Grove");
            if (action == "My house")
                MyMap.MoveToRegion(
                    MapSpan.FromCenterAndRadius(
                        new Position(MyMap.Pins[0].Position.Latitude, MyMap.Pins[0].Position.Longitude), Distance.FromMiles(1)));

            else if (action == "My old house")
                MyMap.MoveToRegion(
                    MapSpan.FromCenterAndRadius(
                        new Position(MyMap.Pins[1].Position.Latitude, MyMap.Pins[1].Position.Longitude), Distance.FromMiles(1)));

            else if (action == "My sister's house")
                MyMap.MoveToRegion(
                    MapSpan.FromCenterAndRadius(
                        new Position(MyMap.Pins[2].Position.Latitude, MyMap.Pins[2].Position.Longitude), Distance.FromMiles(1)));

            else if (action == "Bogan Grove")
                MyMap.MoveToRegion(
                    MapSpan.FromCenterAndRadius(
                        new Position(MyMap.Pins[3].Position.Latitude, MyMap.Pins[3].Position.Longitude), Distance.FromMiles(1)));
        }
    }
}
